import { ApolloServer } from 'apollo-server-lambda';
import { buildFederatedSchema } from '@apollo/federation';

import typeDefs from './graphql/types';
import resolvers from './graphql/resolvers';

const schema = buildFederatedSchema([
  {
    typeDefs,
    resolvers
  }
]);

const server = new ApolloServer({
  schema,
  introspection: true,
  playground: true,
  tracing: true,
  debug: process.env.NODE_ENV === 'development' ? true : false,
  context: ({ event, context }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context
  })
});

exports.graphqlHandler = server.createHandler({
  cors: {
    origin: '*',
    methods: 'GET, POST',
    allowedHeaders:
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  }
});

export { schema, ApolloServer };
