import { gql } from 'apollo-server-lambda';

import defaulTypes from './default.type';

const typeMerged: string[] = [defaulTypes];

const typeDefs = typeMerged.join(' ');

export default gql(typeDefs);
