import { createTestClient } from 'apollo-server-testing';
import gql from 'graphql-tag';

import { constructTestServer } from './__utils';

const GET_ME = gql`
  query {
    me {
      id
      username
    }
  }
`;

describe('Queries', () => {
  it('fetch me', async () => {
    // create an instance of ApolloServer that mocks out context, while reusing
    // existing dataSources, resolvers, and typeDefs.
    // This function returns the server instance as well as our dataSource
    // instances, so we can overwrite the underlying fetchers
    const { server } = constructTestServer();

    // use our test server as input to the createTestClient fn
    // This will give us an interface, similar to apolloClient.query
    // to run queries against our instance of ApolloServer
    const { query } = createTestClient(server);
    const res = await query({ query: GET_ME });
    expect(res).toMatchSnapshot();
  });
});
