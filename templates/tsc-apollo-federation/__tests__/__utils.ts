import faker from 'faker';
import { schema, ApolloServer } from '../src';

const mocks = {
  User: () => ({
    id: faker.random.uuid(),
    username: faker.internet.userName()
  })
};

/**
 * Integration testing utils
 */
const constructTestServer = () => {
  const server = new ApolloServer({
    schema,
    mocks
  });

  return { server };
};

export { constructTestServer };
